package com.jiuri.proxy.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.jiuri.proxy.async.CheckProxyAsync;
import com.jiuri.proxy.config.Ip66Config;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-25 17:51
 **/
@Service
public class Ip66ProxyService {
    private static Logger LOGGER = LoggerFactory.getLogger(Ip66ProxyService.class);
    @Autowired
    private Ip66Config ip66;
    @Autowired
    private CheckProxyAsync checkProxyAsync;
    @Async
    public void ip66Proxy(String url) {
        HttpResponse response;
        try {
            response = HttpUtil.createGet(url).cookie(ip66.getCookie()).header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36").timeout(ip66.getTimeout()).execute();
        }catch (Exception e){
            LOGGER.info("ip66请求失败,url:{}",url);
            return;
        }
        if (!response.isOk()) {
            if (response.getStatus()==521) {
                LOGGER.info("cookie过期");
                return;
            }
        }
        Document document = Jsoup.parse(response.body());
        Elements table = document.select("table tr");
        table.remove(0);
        table.remove(0);
        table.remove(0);
        int i = 0;
        for (Element element : table) {
            i++;
            if(ip66.getEndPageNum()==i){
                return;
            }
            String ip = element.select("td:nth-child(1)").text();
            Integer port = Integer.parseInt(element.select("td:nth-child(2)").text());
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            checkProxyAsync.check(ip, port);
        }
        if(!url.contains("areaindex_35")){
            String next_page = document.getElementById("PageList").select("a").last().attr("href");
            Integer pageNum = Integer.parseInt(next_page.replaceAll("/","").replaceAll("\\.html",""));
            if(ip66.getEndPageNum()==pageNum){
                LOGGER.info("ip66 --- end");
                return;
            }
            LOGGER.info("66ip --- get page : "+ip66.getHost() + next_page);
            ip66Proxy(ip66.getHost() + next_page);
        }
    }
}
