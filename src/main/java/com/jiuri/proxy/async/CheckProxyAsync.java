package com.jiuri.proxy.async;

import cn.hutool.http.HttpException;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.jiuri.proxy.dao.ProxyDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import redis.clients.jedis.exceptions.JedisConnectionException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.NoSuchElementException;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-27 10:36
 **/
@Component
public class CheckProxyAsync {
    private static Logger LOGGER = LoggerFactory.getLogger(CheckProxyAsync.class);
    @Value("${proxy.timeout}")
    private Integer timeout;
    @Autowired
    private ProxyDAO proxyDAO;
    @Async
    public void check(String host,Integer port){
        Proxy proxy = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(host, port));
        HttpResponse execute;
        String ip = host+":"+port;
        try {
            execute = HttpUtil.createGet("https://www.qq.com/").timeout(timeout).setProxy(proxy).execute();
        }catch (HttpException e){
            proxyDAO.remove(ip);
            return;
        }catch (NoSuchElementException e){
            return;
        }catch (JedisConnectionException e){
            LOGGER.error("redis连接超时",e);
            return;
        }
        if(execute.isOk()){
            proxyDAO.save(ip);
            return;
        }
    }
}
