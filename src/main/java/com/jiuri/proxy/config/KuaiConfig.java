package com.jiuri.proxy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("proxy.kuai")
public class KuaiConfig extends BaseConfig {

}