package com.jiuri.proxy.task;

import com.jiuri.proxy.config.Ip66Config;
import com.jiuri.proxy.config.Ip89Config;
import com.jiuri.proxy.config.KuaiConfig;
import com.jiuri.proxy.config.XiciConfig;
import com.jiuri.proxy.dao.ProxyDAO;
import com.jiuri.proxy.service.Ip66ProxyService;
import com.jiuri.proxy.service.Ip89ProxyService;
import com.jiuri.proxy.service.KuaiProxyService;
import com.jiuri.proxy.service.XiciProxyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-27 15:30
 **/
@Component
public class SpiderTask {
    @Autowired
    private Ip66Config ip66Config;
    @Autowired
    private Ip89Config ip89Config;
    @Autowired
    private KuaiConfig kuaiConfig;
    @Autowired
    private XiciConfig xiciConfig;
    @Autowired
    private Ip66ProxyService ip66ProxyService;
    @Autowired
    private Ip89ProxyService ip89ProxyService;
    @Autowired
    private KuaiProxyService kuaiProxyService;
    @Autowired
    private XiciProxyService xiciProxyService;
    @Autowired
    private ProxyDAO proxyDAO;

    @Scheduled(initialDelay = 2 * 1000,fixedRate = 10 * 60 * 1000)
    public void spider(){
        if(ip66Config.getSpiderSwitch()==1){
            ip66ProxyService.ip66Proxy(ip66Config.getHost()+"/"+ip66Config.getStartPageNum()+".html");
        }
        if(ip89Config.getSpiderSwitch()==1){
            ip89ProxyService.ip89Proxy(ip89Config.getHost()+"/index_"+ip89Config.getStartPageNum()+".html");
        }
        if(kuaiConfig.getSpiderSwitch()==1){
            kuaiProxyService.kuaiProxy(kuaiConfig.getHost()+"/free/inha/"+kuaiConfig.getStartPageNum());
        }
        if(xiciConfig.getSpiderSwitch()==1){
            String proxy = proxyDAO.random();
            if(StringUtils.isBlank(proxy)){
                return;
            }
            xiciProxyService.xiciProxy(xiciConfig.getHost()+"/nn/"+xiciConfig.getStartPageNum(),proxy);
        }
    }
}
